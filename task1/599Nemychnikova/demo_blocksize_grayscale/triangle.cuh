#pragma once

#include <glm/glm.hpp>
#include <common/common_functions.cuh>

void triangle(glm::u8vec4* dst_image, ImageWorkArea srcWorkArea, ImageWorkArea dstWorkArea, glm::uvec2 blockDim,
              glm::uvec2 lower_left, glm::uvec2 upper_right,
              glm::uvec2 v1,  glm::uvec2 v2,  glm::uvec2 v3,
              glm::uvec4 v1_col, glm::uvec4 v2_col, glm::uvec4 v3_col);
