#include "surface_generator.h"
#include <iostream>

//int vertex_height(glm::uvec2 v){
//    return (v.x * v.x + v.y * v.y) / 10000;
//}

int vertex_height(glm::uvec2 v){
    return int(fabsf(sinf(v.x)) * 255.0);
}


glm::uvec4 get_color_from_height(int h){
    std::cout << h << "\n";
    if (h > 255){
        h = 255;
    }
    return {15, 15, h, 255};
}

std::vector<Triangle> SurfaceGenerator::sea_surface(){
    std::vector<Triangle> surface;
    int step = 50;
    double step_irr = step * 0.866; // sqrt(3) / 2

    for (int cur_v_x = 0; cur_v_x < W; cur_v_x += step){
        for (int cur_v_y = 0; cur_v_y < H; cur_v_y += 2 * step_irr){
            Triangle t;
            t.vert = {{cur_v_x, cur_v_y}, {cur_v_x - step / 2, cur_v_y + step_irr},  {cur_v_x + step / 2, cur_v_y + step_irr}};
            for(int i = 0; i < 3; ++i) {
                t.cols[i] = get_color_from_height(vertex_height(t.vert[i]));
            }
            surface.push_back(t);
        }
        for (int cur_v_y = 2 *step_irr; cur_v_y < H; cur_v_y += 2 * step_irr){
            Triangle t;
            t.vert = {{cur_v_x, cur_v_y}, {cur_v_x - step / 2, cur_v_y - step_irr},  {cur_v_x + step / 2, cur_v_y - step_irr}};
            for(int i = 0; i < 3; ++i) {
                t.cols[i] = get_color_from_height(vertex_height(t.vert[i]));
            }
            surface.push_back(t);
        }
        for (int cur_v_y = 0; cur_v_y < H; cur_v_y += 2 * step_irr){
            Triangle t;
            t.vert = {{cur_v_x, cur_v_y}, {cur_v_x + step, cur_v_y},  {cur_v_x + step / 2, cur_v_y + step_irr}};
            for(int i = 0; i < 3; ++i) {
                t.cols[i] = get_color_from_height(vertex_height(t.vert[i]));
            }
            surface.push_back(t);
        }
        for (int cur_v_y = 0; cur_v_y < H; cur_v_y += 2 * step_irr){
            Triangle t;
            t.vert = {{cur_v_x, cur_v_y}, {cur_v_x + step / 2, cur_v_y - step_irr},  {cur_v_x + step, cur_v_y}};
            for(int i = 0; i < 3; ++i) {
                t.cols[i] = get_color_from_height(vertex_height(t.vert[i]));
            }
            surface.push_back(t);
        }
    }

    return surface;
}