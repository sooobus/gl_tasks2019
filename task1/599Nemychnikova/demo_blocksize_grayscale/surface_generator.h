#ifndef PROJECT_SURFACE_GENERATOR_H
#define PROJECT_SURFACE_GENERATOR_H

#endif //PROJECT_SURFACE_GENERATOR_H

#include <vector>
#include <glm/glm.hpp>
#include "triangle_service.h"
//#include "triangle_service.h"

class SurfaceGenerator{
public:
    SurfaceGenerator(unsigned int W_, unsigned int H_) : W(W_), H(H_){}
    ~SurfaceGenerator(){};

    std::vector<Triangle> sea_surface();

private:
    unsigned int W, H;
};