#include <common/common_functions.cu>
#include <common/CudaError.h>

#include <common/CudaForwardDefines.h>

#define SIGN( a, b, c) (((a).x - (c).x) * ((b).y - (c).y) - ((b).x - (c).x) * ((a).y - (c).y))
#define DIST(a, b) (((((a).x) - ((b).x)) * (((a).x) - ((b).x)) + (((a).y) - ((b).y)) * (((a).y) - ((b).y))))
#define DOT(a, b) ((a).x * (b).x + (a).y * (b).y)


// This function is executed on DEVICE and can be called both from HOST and DEVICE.
__global__ void _triangle(glm::u8vec4* dst_image, ImageWorkArea srcWorkArea, ImageWorkArea dstWorkArea,
                          glm::uvec2 lower_left, glm::uvec2 upper_right, glm::uvec2 v1,  glm::uvec2 v2,  glm::uvec2 v3,
                          glm::uvec4 v1_col, glm::uvec4 v2_col, glm::uvec4 v3_col) {
	glm::ivec2 workPixel(threadIdx.x + blockDim.x * blockIdx.x, threadIdx.y + blockDim.y * blockIdx.y);

	int srcOffset = count_img_offset(workPixel, srcWorkArea);
	int dstOffset = count_img_offset(workPixel, dstWorkArea);

	if (srcOffset < 0 || dstOffset < 0)
		return;

	if (workPixel.x < lower_left.x || workPixel.y < lower_left.y || workPixel.x > upper_right.x || workPixel.y > upper_right.y)
	    return;

    // Using method that checks on which side of each half-plane of triangle sides the point lives,
    // see https://www.gamedev.net/forums/topic/295943-is-this-a-better-point-in-triangle-test-2d/

    int d1 = SIGN(workPixel, v1, v2);
    int d2 = SIGN(workPixel, v2, v3);
    int d3 = SIGN(workPixel, v3, v1);

    bool has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
    bool has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

    if (has_neg && has_pos)
        return;

    // Calculate distances to each vertex to assign color according to it.

	glm::ivec2 r1 = glm::ivec2(v1) - workPixel;
    glm::ivec2 r2 = glm::ivec2(v2) - workPixel;
    glm::ivec2 r3 = glm::ivec2(v3) - workPixel;
	float dist1 = hypotf(float(r1.x), float(r1.y));
    float dist2 = hypotf(float(r2.x), float(r2.y));
    float dist3 = hypotf(float(r3.x), float(r3.y));

	float sum = dist1 + dist2 + dist3;
	dist1 /= sum;
	dist2 /= sum;
	dist3 /= sum;

    //glm::vec2 h0 = {v2.x - v1.x, v2.y - v1.y};
    //glm::vec2 h1 = {v3.x - v1.x, v3.y - v1.y};
    //glm::vec2 h2 = {workPixel.x - v1.x, workPixel.y - v1.y};

    //float d00 = glm::dot(h0, h0);
    //float d01 = glm::dot(h0, h1);
    //float d11 = glm::dot(h1, h1);
    //float d20 = glm::dot(h2, h0);
    //float d21 = glm::dot(h2, h1);
    //float denominator = d00 * d11 - d01 * d01;
    //float beta  = (d11 * d20 - d01 * d21) / denominator;
    //float gamma = (d00 * d21 - d01 * d20) / denominator;

    //float dist1 = 1.0f - beta - gamma;
    //float dist2 = beta;
    //float dist3 = gamma;

	glm::u8vec4 res_color = {0, 0, 0, 0};

    int v1_col_x = int(float(v1_col.x) * dist1);
    int v1_col_y = int(float(v1_col.y) * dist1);
    int v1_col_z = int(float(v1_col.z) * dist1);
    int v1_col_t = 255;

    int v2_col_x = int(float(v2_col.x) * dist2);
    int v2_col_y = int(float(v2_col.y) * dist2);
    int v2_col_z = int(float(v2_col.z) * dist2);
    int v2_col_t = 255;

    int v3_col_x = int(float(v3_col.x) * dist3);
    int v3_col_y = int(float(v3_col.y) * dist3);
    int v3_col_z = int(float(v3_col.z) * dist3);
    int v3_col_t = 255;

	dst_image[dstOffset] = glm::u8vec4({v1_col_x + v2_col_x + v3_col_x,
                                        v1_col_y + v2_col_y + v3_col_y,
                                        v1_col_z + v2_col_z + v3_col_z,
                                        v1_col_t + v2_col_t + v3_col_t});
}

// This is wrapper function for calling DEVICE _grayscale function from HOST.
void triangle(glm::u8vec4* dst_image, ImageWorkArea srcWorkArea, ImageWorkArea dstWorkArea, glm::uvec2 blockDim,
              glm::uvec2 lower_left, glm::uvec2 upper_right, glm::uvec2 v1,  glm::uvec2 v2,  glm::uvec2 v3,
              glm::uvec4 v1_col, glm::uvec4 v2_col, glm::uvec4 v3_col) {

	dim3 blkDim(blockDim.x, blockDim.y, 1);
	glm::uvec2 gridDim = glm::uvec2(glm::ivec2(srcWorkArea.workDim + blockDim) - 1) / blockDim;
	dim3 grdDim = { gridDim.x, gridDim.y, 1 };

	_triangle<<<grdDim, blkDim>>>(dst_image, srcWorkArea, dstWorkArea, lower_left, upper_right, v1, v2, v3, v1_col, v2_col, v3_col);
	checkCudaErrors(cudaGetLastError());
}
