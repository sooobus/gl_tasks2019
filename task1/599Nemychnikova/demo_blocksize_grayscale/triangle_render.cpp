#include <cudasoil/cuda_soil.h>
#include <common/CudaImage.h>
#include <common/CudaTimeMeasurement.h>
#include <common/CudaInfo.h>

#include <vector>

#include "triangle.cuh"
//#include "triangle_service.h"
#include "surface_generator.h"
//#include "surface_generator.cpp"


int main() {
	printCudaInfo();
	cudaDeviceProp deviceProp;
	cudaGetDeviceProperties(&deviceProp, 0);

    const size_t W = 800;
    const size_t H = 600;
    SurfaceGenerator gen(W, H);
    std::cout << "Loading triangles" << std::endl;
    auto triangles = gen.sea_surface();

    std::cout << "Loaded triangles" << std::endl;

	auto dstImage = std::make_shared<CudaImage4c>(W, H, ResourceLocation::Device);
    ImageWorkArea workArea(glm::uvec2(W, H));

	struct TimeReport {
	    TimeReport(float t, size_t m, glm::uvec2 bs) : totalTime(t), measurements(m), blockSize(bs){};
		float totalTime = 0.0f;
		size_t measurements = 0;
		glm::uvec2 blockSize;
	};

	std::vector<TimeReport> results;


	CudaTimeMeasurement timer;
	auto runTask = [&](glm::uvec2 blockSize, size_t measureId) {
		cudaMemset(dstImage->getHandle(), 0, dstImage->getSize());

		timer.start();
		for (auto& tri : triangles) {
			auto coords = get_bounding_rect(W, H, tri);
			auto c1 = coords.first;
			auto c2 = coords.second;
			triangle(dstImage->getHandle(), workArea, workArea, blockSize, c1, c2, tri.vert[0], tri.vert[1],
					 tri.vert[2], tri.cols[0], tri.cols[1], tri.cols[2]);
		}
		timer.stop();

		if (results.size() <= measureId) {
		    TimeReport measurement(0.0f, 0, blockSize);
			results.resize(measureId + 1, measurement);
		}
		results[measureId].totalTime += timer.getSeconds();
		results[measureId].measurements++;
	};

    runTask(glm::u8vec2(32, 8), 0);

    /*
    for (int measurement = 0; measurement < 25; measurement++) {
		size_t measureId = 0;

		for (unsigned int i = 0u; i < 7u; i++) {
			for (unsigned int j = 0u; j < 7u; j++) {
				glm::uvec2 blockSize = {1u << i, 1u << j};
				if (blockSize.x * blockSize.y > deviceProp.maxThreadsPerBlock) {
					break;
				}

				runTask(blockSize, measureId);

				++measureId;
			}
		}

		// Non-power-of-two block sizes.
		runTask(glm::u8vec2(7, 5), measureId);
		++measureId;
		runTask(glm::u8vec2(17, 13), measureId); // block has almost same size as 32x8, compare time now.
		++measureId;
	}
*/
	for (const auto &result : results) {
		std::cout << "Grayscale:\tblock size\t" << result.blockSize.x << "x"
		<< result.blockSize.y << "\t\ttime " << result.totalTime / result.measurements << "s" << std::endl;
	}

	save_image_rgb("599NemychnikovaData1/out_image.png", dstImage);

	return 0;
}
