#include "triangle_service.h"

std::pair<glm::uvec2, glm::uvec2> get_bounding_rect(const unsigned int W, const unsigned int H, Triangle t){
    glm::uvec2 lower_left = {W, H};
    glm::uvec2 upper_right = {0, 0};

    for (auto &v : t.vert){
        if (v.x < lower_left.x){
            lower_left.x = v.x;
        }
        if (v.y < lower_left.y){
            lower_left.y = v.y;
        }
        if (v.x > upper_right.x){
            upper_right.x = v.x;
        }
        if (v.y > upper_right.y){
            upper_right.y = v.y;
        }
    }
    return {lower_left, upper_right};
}