#ifndef PROJECT_TRIANGLE_SERVICE_H
#define PROJECT_TRIANGLE_SERVICE_H

#endif //PROJECT_TRIANGLE_SERVICE_H

#include <vector>
#include <glm/glm.hpp>

struct Triangle{
    std::vector<glm::uvec2> vert;
    std::vector<glm::uvec4> cols = {{100, 50, 10, 100}, {100, 50, 10, 100}, {100, 50, 10, 100}};
};

// Returns {x_0, y_0, x_1, y_1} -- coords of left lower and right upper corners of the rectangle
std::pair<glm::uvec2, glm::uvec2> get_bounding_rect(const unsigned int W, const unsigned int H, Triangle t);