#include <cudasoil/cuda_soil.h>
#include <common/CudaImage.h>
#include <common/CudaTimeMeasurement.h>
#include <common/CudaInfo.h>

#include <vector>

#include "grayscale.cuh"

int main() {
	printCudaInfo();
	cudaDeviceProp deviceProp;
	cudaGetDeviceProperties(&deviceProp, 0);

	auto sourceImage = load_image_rgb("123IvanovData1/image_8k.png");
	auto dstImage = std::make_shared<CudaImage4c>(sourceImage->getWidth(), sourceImage->getHeight(), ResourceLocation::Device);
	ImageWorkArea workArea(glm::uvec2(sourceImage->getWidth(), sourceImage->getHeight()));


	struct TimeReport {
	    TimeReport(float t, size_t m, glm::uvec2 bs) : totalTime(t), measurements(m), blockSize(bs){};
		float totalTime = 0.0f;
		size_t measurements = 0;
		glm::uvec2 blockSize;
	};

	std::vector<TimeReport> results;


	CudaTimeMeasurement timer;
	auto runTask = [&](glm::uvec2 blockSize, size_t measureId) {
		cudaMemset(dstImage->getHandle(), 0, dstImage->getSize());

		timer.start();
		grayscale(sourceImage->getHandle(), dstImage->getHandle(), workArea, workArea, blockSize);
		timer.stop();

		if (results.size() <= measureId) {
		    TimeReport measurement(0.0f, 0, blockSize);
			results.resize(measureId + 1, measurement);
		}
		results[measureId].totalTime += timer.getSeconds();
		results[measureId].measurements++;
	};

	for (int measurement = 0; measurement < 25; measurement++) {
		size_t measureId = 0;

		for (unsigned int i = 0u; i < 7u; i++) {
			for (unsigned int j = 0u; j < 7u; j++) {
				glm::uvec2 blockSize = {1u << i, 1u << j};
				if (blockSize.x * blockSize.y > deviceProp.maxThreadsPerBlock) {
					break;
				}

				runTask(blockSize, measureId);

				++measureId;
			}
		}

		// Non-power-of-two block sizes.
		runTask(glm::u8vec2(7, 5), measureId);
		++measureId;
		runTask(glm::u8vec2(17, 13), measureId); // block has almost same size as 32x8, compare time now.
		++measureId;
	}

	for (const auto &result : results) {
		std::cout << "Grayscale:\tblock size\t" << result.blockSize.x << "x"
		<< result.blockSize.y << "\t\ttime " << result.totalTime / result.measurements << "s" << std::endl;
	}

	save_image_rgb("123IvanovData1/out_image_8k_grayscale.png", dstImage);

	return 0;
}
